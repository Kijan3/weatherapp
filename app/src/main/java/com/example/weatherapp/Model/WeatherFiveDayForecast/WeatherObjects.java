package com.example.weatherapp.Model.WeatherFiveDayForecast;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class WeatherObjects {

    @SerializedName("dt")
    private long dayTime;
    @SerializedName("main")
    private WeatherMainInfo mainInfo;
    @SerializedName("weather")
    private Weather[] weathers;
    private Clouds clouds;
    private Wind wind;
    private Rain rain;
    @SerializedName("sys")
    private System system;
    @SerializedName("dt_txt")
    private String dateText;

    public WeatherObjects() {}

    public WeatherObjects(long dayTime, WeatherMainInfo mainInfo,
                          Weather[] weathers, Clouds clouds, Wind wind,
                          Rain rain, System system, String dateText) {
        this.dayTime = dayTime;
        this.mainInfo = mainInfo;
        this.weathers = weathers;
        this.clouds = clouds;
        this.wind = wind;
        this.rain = rain;
        this.system = system;
        this.dateText = dateText;
    }

    @Override
    public String toString() {
        return "WeatherObjects{" +
                "dayTime=" + dayTime +
                ", mainInfo=" + mainInfo +
                ", weathers=" + Arrays.toString(weathers) +
                ", clouds=" + clouds +
                ", wind=" + wind +
                ", rain=" + rain +
                ", system=" + system +
                ", dateText='" + dateText + '\'' +
                '}';
    }

    public long getDayTime() {
        return dayTime;
    }

    public void setDayTime(long dayTime) {
        this.dayTime = dayTime;
    }

    public WeatherMainInfo getMainInfo() {
        return mainInfo;
    }

    public void setMainInfo(WeatherMainInfo mainInfo) {
        this.mainInfo = mainInfo;
    }

    public Weather[] getWeathers() {
        return weathers;
    }

    public void setWeathers(Weather[] weathers) {
        this.weathers = weathers;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public String getDateText() {
        return dateText;
    }

    public void setDateText(String dateText) {
        this.dateText = dateText;
    }
}
