package com.example.weatherapp;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class WeatherUtilities {

    public String windDirection(double degree){

        String windDirection = "";

        if (degree > 338 || degree <= 23){
            windDirection = "N";
        } else if (degree > 23 && degree <= 68){
            windDirection = "NE";
        } else if (degree > 68 && degree <= 113){
            windDirection = "E";
        } else if (degree > 113 && degree <= 158){
            windDirection = "SE";
        }else if (degree > 158 && degree <= 203){
            windDirection = "S";
        } else if (degree > 203 && degree <= 248){
            windDirection = "SW";
        } else if (degree > 248 && degree <= 293){
            windDirection = "W";
        } else if (degree > 293 && degree <= 338){
            windDirection = "NW";
        }
        return windDirection;
    }

    public int calvinToCelsius(double f){
        double celsius = f - 273.15;
        return (int) Math.round(celsius);
    }


    public String timeFromMiliseconds(long time){
        Date dateToTime = new Date(time * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(dateToTime);
    }



}
