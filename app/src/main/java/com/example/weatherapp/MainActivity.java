package com.example.weatherapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.weatherapp.Model.WeatherFiveDayForecast.WeatherFiveDayForecast;
import com.example.weatherapp.Model.WeatherNow.WeatherStatistics;
import com.example.weatherapp.UseCases.WeatherUsecase;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ImageView iconImageView;

    TextView temperatureTextView, weatherDescriptionTextView, windDirectionAndSpeedTextView,
            pressureTextView, humidityTextView, visibilityTextView, longitudeTextView, latitudeTextView,
            cityStationTextView, sunriseTextView, sunsetTextView;


    LocationManager lm;


    int permisionLocalizationCheck, permissionInternetCheck;

    private long backPressedTime;


    public static double longitude;
    public static double latitude;

    public WeatherStatistics ws;
    public WeatherFiveDayForecast wfdf;
    private WeatherUtilities weatherUtilities;
    private WeatherUsecase weatherUsecase;

    RequestQueue requestQueue;
    RequestQueue requestQueueFiveDay;

    ScrollView backgroundLayout;
    AnimationDrawable animationDrawable;

    //=====================
    List<String> titlesList;
    //=====================


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iconImageView = findViewById(R.id.weather_icon_view);
        temperatureTextView = findViewById(R.id.text_view_temperature);
        weatherDescriptionTextView = findViewById(R.id.text_view_weather_description);
        windDirectionAndSpeedTextView = findViewById(R.id.text_view_wind_speed_and_direction);
        pressureTextView = findViewById(R.id.text_view_pressure);
        humidityTextView = findViewById(R.id.text_view_humidity);
        visibilityTextView = findViewById(R.id.text_view_visibility);
        longitudeTextView = findViewById(R.id.text_view_longitude);
        latitudeTextView = findViewById(R.id.text_view_latitude);
        cityStationTextView = findViewById(R.id.text_view_city_station);
        sunriseTextView = findViewById(R.id.text_view_sunrise);
        sunsetTextView = findViewById(R.id.text_view_city_sunset);

        backgroundLayout = findViewById(R.id.background_animation_layout);
        animationDrawable = (AnimationDrawable) backgroundLayout.getBackground();
        animationDrawable.setEnterFadeDuration(10);
        animationDrawable.setExitFadeDuration(5000);

        permisionLocalizationCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        permissionInternetCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        weatherUtilities = new WeatherUtilities();
        weatherUsecase = new WeatherUsecase();

        requestQueue = Volley.newRequestQueue(this);
        requestQueueFiveDay = Volley.newRequestQueue(this);

        //==================================

        titlesList = Arrays.asList("Pogoda dzisiaj", "Pogoda jutro", "Pogoda pojutrze",
                "Pogoda popojutrze", "Pogoda popopojutrze");

        //==================================


        startAnimatedBackground();
        getCoordinates();
        getCoordinatesTwo();

    }

    private void startAnimatedBackground() {
        animationDrawable.start();
    }

    @SuppressLint("SetTextI18n")
    private void setTextViews(WeatherStatistics ws) {
        iconSetter(ws.getWeather()[0].getIcon());
        temperatureTextView.setText(weatherUtilities.calvinToCelsius(ws.getMainWeatherInfo().getTemperature()) + "\u00B0C");
        weatherDescriptionTextView.setText(ws.getWeather()[0].getDescription());
        windDirectionAndSpeedTextView.setText("Wiatr: " + ws.getWind().getSpeed() + " m/s, Kierunek: "
                + weatherUtilities.windDirection(ws.getWind().getDeg()));
        pressureTextView.setText("Ciśnienie: " + ws.getMainWeatherInfo().getPressure() + " hPa");
        humidityTextView.setText("Wilgotność: " + ws.getMainWeatherInfo().getHumidity() + " %");
        visibilityTextView.setText("Widoczność: " + ws.getVisibility() + " m");
        longitudeTextView.setText("Lon: " + longitude);
        latitudeTextView.setText("Lat: " + latitude);
        cityStationTextView.setText(ws.getName() + ", " + ws.getSystem().getCountry());
        sunriseTextView.setText("Wschód: " + weatherUtilities.timeFromMiliseconds(ws.getSystem().getSunrise()));
        sunsetTextView.setText("Zachód: " + weatherUtilities.timeFromMiliseconds(ws.getSystem().getSunset()));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_weather_forecast, menu);
        // TESTY ==============================================
        MenuItem itemOne = menu.findItem(R.id.item_two);
        MenuItem itemTwo = menu.findItem(R.id.item_three);
        MenuItem itemThree = menu.findItem(R.id.item_four);
        MenuItem itemFour = menu.findItem(R.id.item_five);
        MenuItem itemFive = menu.findItem(R.id.item_six);
        itemOne.setTitle(titlesList.get(0));
        itemTwo.setTitle(titlesList.get(1));
        itemThree.setTitle(titlesList.get(2));
        itemFour.setTitle(titlesList.get(3));
        itemFive.setTitle(titlesList.get(4));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_two:
                openNewWindow();
                // open new Window
                return true;
            case R.id.item_three:
                Toast.makeText(this, "item two", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_four:
                Toast.makeText(this, "item three", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_five:
                Toast.makeText(this, "item four", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_six:
                Toast.makeText(this, "item five", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openNewWindow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        View addView = LayoutInflater.from(this).inflate(R.layout.prognoza, null);
        builder.setPositiveButton("Zamknij", null);
        builder.setView(addView);
        builder.create();
        builder.show();
    }


    private void getWeatherNowJson(double lon, double lat) {
        String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon +
                "&APPID=xxx";
        JsonObjectRequest jor = new JsonObjectRequest(
                Request.Method.GET,
                url,
                (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ws = createWeather(response.toString());
                        setTextViews(ws);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Wystąpił błąd " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                }
        );
        requestQueue.add(jor);
    }

    private void getFiveDayWeatherJson(double lon, double lat) {
        String url = "https://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lon +
                "&APPID=xxx";

        JsonObjectRequest jor = new JsonObjectRequest(
                Request.Method.GET,
                url,
                (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        wfdf = createFiveDayWeatherForecast(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );
    }

    private List<String> menuTitlesList(WeatherFiveDayForecast wfdf) {
        List<String> mtl = new ArrayList<>();

        //==========TU SKONCZYLEM ================================================================================
        //Zrobić listę dat prognozy, która będzie się wyśiwtlała jako menuitemy :)
        //===================================================================


        return null;
    }


    private void iconSetter(String icon) {
        switch (icon) {
            case "01d":
                iconImageView.setImageResource(R.drawable.zero_one_day);
                break;
            case "01n":
                iconImageView.setImageResource(R.drawable.zero_one_night);
                break;
            case "02d":
                iconImageView.setImageResource(R.drawable.zero_two_day);
                break;
            case "02n":
                iconImageView.setImageResource(R.drawable.zero_two_night);
                break;
            case "03d":
                iconImageView.setImageResource(R.drawable.zero_three_day);
                break;
            case "03n":
                iconImageView.setImageResource(R.drawable.zero_three_night);
                break;
            case "04d":
                iconImageView.setImageResource(R.drawable.zero_four_day);
                break;
            case "04n":
                iconImageView.setImageResource(R.drawable.zero_four_night);
                break;
            case "09d":
                iconImageView.setImageResource(R.drawable.zero_nine_day);
                break;
            case "09n":
                iconImageView.setImageResource(R.drawable.zero_nine_night);
                break;
            case "10d":
                iconImageView.setImageResource(R.drawable.zero_ten_day);
                break;
            case "10n":
                iconImageView.setImageResource(R.drawable.zero_ten_night);
                break;
            case "11d":
                iconImageView.setImageResource(R.drawable.zero_eleven_day);
                break;
            case "11n":
                iconImageView.setImageResource(R.drawable.zero_eleven_night);
                break;
            case "13d":
                iconImageView.setImageResource(R.drawable.zero_thirteen_day_night);
                break;
            case "13n":
                iconImageView.setImageResource(R.drawable.zero_thirteen_day_night);
                break;
            case "50d":
                iconImageView.setImageResource(R.drawable.fifty_mist_day_night);
                break;
            case "50n":
                iconImageView.setImageResource(R.drawable.fifty_mist_day_night);
                break;
            default:
                iconImageView.setImageResource(R.drawable.zero_one_day);
        }
    }


    public void getCoordinates() {
        if (permisionLocalizationCheck == 0) {
            @SuppressLint("MissingPermission")
            Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            getWeatherNowJson(longitude, latitude);
//            connectionIndicator.setVisibility(View.INVISIBLE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    @SuppressLint("MissingPermission")
    private void getCoordinatesTwo() {
        if (permisionLocalizationCheck == 0) {
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    getWeatherNowJson(longitude, latitude);

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {
//                    Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                    longitude = location.getLongitude();
//                    latitude = location.getLatitude();
                }
            });
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    // CREATE WEATHER NOW =========================================
    public WeatherStatistics createWeather(String weatherJson) {
        WeatherStatistics ws;
        if (permissionInternetCheck == 0) {
            ws = weatherUsecase.weatherStatistics(weatherJson);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 1);
            ws = null;
        }
        return ws;
    }

    // CREATE 5 day forecast ============================
    public WeatherFiveDayForecast createFiveDayWeatherForecast(String fiveDayWeatherJson) {
        WeatherFiveDayForecast fiveDay;
        if (permissionInternetCheck == 0) {
            fiveDay = weatherUsecase.weatherFiveDayForecast(fiveDayWeatherJson);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 1);
            fiveDay = null;
        }
        return fiveDay;
    }


//    private void pushButton(){
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getJson(longitude, latitude);
//
//            }
//        });
//    }

//    private void clock(){
//        new Timer().scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                Calendar cal = Calendar.getInstance();
//                cal.getTime();
//                int hour = cal.get(Calendar.HOUR);
//                int min = cal.get(Calendar.MINUTE);
//                int sec = cal.get(Calendar.SECOND);
//                time.setText(hour + ":" + min + ":" + sec);
//            }
//        }, 0,1000);
//    }


//    public boolean isConnected() {
//        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//        if (networkInfo != null && networkInfo.isConnected())
//            return true;
//        else
//            return false;
//    }


    @Override
    public void onBackPressed() {
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            finishApp();
        } else {
            Toast.makeText(this, "Wciśnij ponownie aby zabić aplikację", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    private void finishApp() {
        finish();
    }

}
