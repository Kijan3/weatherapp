# Weather
> IN PROGRESS 
Android mobile application based on the Open Weather REST API showing the weather in the current location as well as a five-day forecast divided into days and hours.
## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
The weather application displays the weather based on the user's location. Shows weather graphic icon, temperature, short description, pressure, wind speed and direction, humidity and visibility. 
Location data are also displayed: latitude and longitude, name of the weather station from which the data is downloaded, as well as sunrise and sunset time.
Three color gradient animation as background.

## Screenshots
![screenshot one](app/screenshots/Screenshot_one.png)
![screenshot two](app/screenshots/Screenshot_two.png)

## Technologies
* Android Studio 3.4.1
* JRE: 1.8.0_152
* JVM: OpenJDK 

## Features
To-do list:
* Automatic topping up of the menu items with dates five days ahead.
* Clicking on a given day opens a window with information about the weather for that day divided into hours. The window should contain a weather icon and some basic information such as temperature, rain and pressure

## Status
Project is: _in progress_
## Contact
Created by MKijanski.
