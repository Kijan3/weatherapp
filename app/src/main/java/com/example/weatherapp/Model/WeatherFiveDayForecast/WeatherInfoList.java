package com.example.weatherapp.Model.WeatherFiveDayForecast;

import com.google.gson.annotations.SerializedName;

public class WeatherInfoList {

    @SerializedName("dt")
    private long dayTime;
    private WeatherMainInfo weatherMainInfo;
    private Weather[] weatherArr;
    private Clouds clouds;
    private Wind wind;
    @SerializedName("sys")
    private System system;
    @SerializedName("dt_txt")
    private String dayTxt;



}
