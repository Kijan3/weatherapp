package com.example.weatherapp.UseCases;

import com.example.weatherapp.Model.WeatherFiveDayForecast.WeatherFiveDayForecast;
import com.example.weatherapp.Model.WeatherNow.WeatherStatistics;
import com.google.gson.Gson;

public class WeatherUsecase {

    Gson gson = new Gson();

    public WeatherStatistics weatherStatistics(String weatherResponse) {
        return gson.fromJson(weatherResponse, WeatherStatistics.class);
    }

    public WeatherFiveDayForecast weatherFiveDayForecast(String forecastResponse) {
        return gson.fromJson(forecastResponse, WeatherFiveDayForecast.class);
    }


}
