package com.example.weatherapp.Model.WeatherFiveDayForecast;

public class System {

    private String pod;

    public System() {}

    public System(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
