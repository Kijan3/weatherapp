package com.example.weatherapp.Model.WeatherFiveDayForecast;

public class Rain {

    private double threeHour;

    public Rain() {}

    public Rain(double threeHour) {
        this.threeHour = threeHour;
    }

    @Override
    public String toString() {
        return "Rain{" +
                "threeHour=" + threeHour +
                '}';
    }

    public double getThreeHour() {
        return threeHour;
    }

    public void setThreeHour(double threeHour) {
        this.threeHour = threeHour;
    }
}
