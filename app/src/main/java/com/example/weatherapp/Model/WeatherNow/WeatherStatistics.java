package com.example.weatherapp.Model.WeatherNow;

import com.google.gson.annotations.SerializedName;

public class WeatherStatistics {

    @SerializedName("coord")
    private Coordinates coordinates;
    private Weather[] weather;
    private String base;
    @SerializedName("main")
    private MainWeatherInfo mainWeatherInfo;
    private int visibility;
    private Wind wind;
    private Clouds clouds;
    @SerializedName("dt")
    private long dayTime;
    @SerializedName("sys")
    private System system;
    private long timezone;
    private long id;
    private String name;
    private int cod;

    public WeatherStatistics() {
    }


    public WeatherStatistics(Coordinates coordinates, Weather[] weather,
                             String base, MainWeatherInfo mainWeatherInfo, int visibility,
                             Wind wind, Clouds clouds, long dayTime, System system,
                             long timezone, long id, String name, int cod) {
        this.coordinates = coordinates;
        this.weather = weather;
        this.base = base;
        this.mainWeatherInfo = mainWeatherInfo;
        this.visibility = visibility;
        this.wind = wind;
        this.clouds = clouds;
        this.dayTime = dayTime;
        this.system = system;
        this.timezone = timezone;
        this.id = id;
        this.name = name;
        this.cod = cod;
    }

    @Override
    public String toString() {
        return "WeatherStatistics{" +
                "coordinates=" + coordinates +
                ", weather=" + weather +
                ", base='" + base + '\'' +
                ", mainWeatherInfo=" + mainWeatherInfo +
                ", wind=" + wind +
                ", clouds=" + clouds +
                ", dayTime=" + dayTime +
                ", system=" + system +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                '}';
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }


    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public MainWeatherInfo getMainWeatherInfo() {
        return mainWeatherInfo;
    }

    public void setMainWeatherInfo(MainWeatherInfo mainWeatherInfo) {
        this.mainWeatherInfo = mainWeatherInfo;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public long getDayTime() {
        return dayTime;
    }

    public void setDayTime(long dayTime) {
        this.dayTime = dayTime;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public long getTimezone() {
        return timezone;
    }

    public void setTimezone(long timezone) {
        this.timezone = timezone;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    //    Parameters:
//    coord
//    coord.lon City geo location, longitude
//    coord.lat City geo location, latitude
//    weather (more info Weather condition codes)
//    weather.id Weather condition id
//    weather.main Group of weather parameters (Rain, Snow, Extreme etc.)
//    weather.description Weather condition within the group
//    weather.icon Weather icon id
//    base Internal parameter
//            main
//    main.temp Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
//    main.pressure Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
//    main.humidity Humidity, %
//    main.temp_min Minimum temperature at the moment. This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
//    main.temp_max Maximum temperature at the moment. This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
//    main.sea_level Atmospheric pressure on the sea level, hPa
//    main.grnd_level Atmospheric pressure on the ground level, hPa
//            wind
//    wind.speed Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
//    wind.deg Wind direction, degrees (meteorological)
//    clouds
//    clouds.all Cloudiness, %
//    rain
//    rain.1h Rain volume for the last 1 hour, mm
//    rain.3h Rain volume for the last 3 hours, mm
//            snow
//    snow.1h Snow volume for the last 1 hour, mm
//    snow.3h Snow volume for the last 3 hours, mm
//    dt Time of data calculation, unix, UTC
//            sys
//    sys.type Internal parameter
//    sys.id Internal parameter
//    sys.message Internal parameter
//    sys.country Country code (GB, JP etc.)
//    sys.sunrise Sunrise time, unix, UTC
//    sys.sunset Sunset time, unix, UTC
//    timezone Shift in seconds from UTC
//    id City ID
//    name City name
//    cod Internal parameter
}
