package com.example.weatherapp.Network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {

    public String sendRequest(double lon, double lat) throws IOException {
//        54.82093,18.237336
        URL url = new URL("https://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lon +
                "&APPID=xxx");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("User-Agent", "rest_weather_app");
        con.setRequestMethod("GET");
        String response = readResponse(con);
        System.out.println(con.getResponseCode());
        con.disconnect();
        return response;
    }

    public String sendRequestCurrentWeather(double lon, double lat) throws IOException {
//        54.82093,18.237336
        URL url = new URL("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon +
                "&APPID=xxx");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("User-Agent", "rest_weather_app");
        con.setRequestMethod("GET");
        String response = readResponse(con);
        System.out.println(con.getResponseCode());
        con.disconnect();
        return response;
    }

    private String readResponse(HttpURLConnection con) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((InputStream) con.getContent()));
        String response;
        StringBuffer sb = new StringBuffer();
        while ((response = br.readLine()) != null) {
            sb.append(response);
        }
        return sb.toString();
    }
}
