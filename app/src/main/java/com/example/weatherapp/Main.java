package com.example.weatherapp;

import com.example.weatherapp.Model.WeatherFiveDayForecast.WeatherFiveDayForecast;
import com.example.weatherapp.Model.WeatherFiveDayForecast.WeatherObjects;
import com.example.weatherapp.Model.WeatherNow.WeatherStatistics;
import com.example.weatherapp.Network.HttpClient;
import com.example.weatherapp.UseCases.WeatherUsecase;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        HttpClient hc = new HttpClient();
        // lodz = 19.457216, 51.759445
        // Jastrzebia Gora = 18.238696, 54.8215491
        String wf = hc.sendRequest(18.238696, 54.8215491);
        System.out.println(wf);

        Gson gson = new Gson();
        WeatherFiveDayForecast fiveday = gson.fromJson(wf, WeatherFiveDayForecast.class);
        List<WeatherObjects> fdf = Arrays.asList(fiveday.getWeatherList());

        System.out.println("Lokalizacja: " + fiveday.getCity().getName());

        WeatherUtilities weatherUtilities = new WeatherUtilities();

        for (WeatherObjects wo : fdf) {
            System.out.println("==========================");
            System.out.println("Ikona" + wo.getWeathers()[0].getIcon());
            System.out.println("Data i godzina: " + wo.getDateText());
            System.out.println("Czas: " + weatherUtilities.timeFromMiliseconds(wo.getDayTime()));
            System.out.println("Temperatura: " + weatherUtilities.calvinToCelsius(wo.getMainInfo().getTemp()) + " C");
            System.out.println("Ciśnienie: " + wo.getMainInfo().getPressure() + " hPa");
            System.out.println("Opis pogody: " + wo.getWeathers()[0].getDescription());
            System.out.println("Stan : " + wo.getWeathers()[0].getMain());
            System.out.println("Szybkośc wiatru: " + wo.getWind().getSpeed() + " m/s");
            System.out.println("Kierunek wiatru: " + weatherUtilities.windDirection(wo.getWind().getDeg()));
            if (wo.getRain() == null) {
                System.out.println("brak danych");
            } else {
                System.out.println("Deszcz: " + wo.getRain().getThreeHour());
            }
        }

        //==========================================================================================
        //============================CURRENT WEATHER===============================================
        //==========================================================================================

        // lodz = 19.457216, 51.759445
        // Jastrzebia Gora = 18.238696, 54.8215491
        // Nowy orlean  = -90.070556, 29.964722

        String curWeather = hc.sendRequestCurrentWeather(18.238696, 54.8215491);
        System.out.println(curWeather);


        WeatherStatistics ws = new WeatherUsecase().weatherStatistics(curWeather);

//        System.out.println("koordynaty " + ws.getCoordinates().getLat() + ", " + ws.getCoordinates().getLon());
//        System.out.println("Weather condition id " + ws.getWeather()[0].getId());
//        System.out.println("Group of weather parameters (Rain, Snow, Extreme etc.) " + ws.getWeather()[0].getMain());
//        System.out.println("Weather condition within the group " + ws.getWeather()[0].getDescription());
//        System.out.println("Weather icon id: " + ws.getWeather()[0].getIcon());
//        System.out.println("Stacje " + ws.getBase());
//        System.out.println("Temperature. Unit Default: Kelvin " + ws.getMainWeatherInfo().getTemperature());
//        System.out.println("Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa " + ws.getMainWeatherInfo().getPressure());

//        System.out.println("Humidity, % " + ws.getMainWeatherInfo().getHumidity());
//        System.out.println("Minimum temperature at the moment. " + ws.getMainWeatherInfo().getMinTemperature());
//        System.out.println("Maximum temperature at the moment. " + ws.getMainWeatherInfo().getMaxTemperature());
//        System.out.println("Visibility, meter " + ws.getVisibility());
//        System.out.println("Wind speed. Unit Default: meter/sec " + ws.getWind().getSpeed());
//        System.out.println("Wind direction, degrees (meteorological) " + ws.getWind().getDeg());
//        System.out.println("Zachmurzenie w % " + ws.getClouds().getAll());

//        System.out.println("Godzina " + ws.getDayTime());
//        System.out.println("Type " + ws.getSystem().getType());
//        System.out.println("Id " + ws.getSystem().getId());
//        System.out.println("Internal parameter " + ws.getSystem().getMessage());
//        System.out.println("Country code (GB, JP etc.) " + ws.getSystem().getCountry());
//        System.out.println("Sunrise time, unix, UTC " + ws.getSystem().getSunrise());
//        System.out.println("Sunset time, unix, UTC" + ws.getSystem().getSunset());
//        System.out.println("Timezone: " + ws.getTimezone());
//        System.out.println("Id miasta? " + ws.getId());
//        System.out.println("City name " + ws.getName());
//        System.out.println("Kod odpowiedzi " + ws.getCod());


        WeatherUtilities wu = new WeatherUtilities();

//        System.out.println(wu.timeFromMiliseconds(1566704405));
//        System.out.println(wu.timeFromMiliseconds(1566755931));


//        new Timer().scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                Calendar cal = Calendar.getInstance();
//                cal.getTime();
//                int hour = cal.get(Calendar.HOUR);
//                int min = cal.get(Calendar.MINUTE);
//                int sec = cal.get(Calendar.SECOND);
//                System.out.println(hour + ":" + min + ":" + sec);
//            }
//        }, 0,1000);
    }
}
